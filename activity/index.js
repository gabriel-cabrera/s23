
let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kant0: ["Brock", "Misty"]},

	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(i){
		console.log(this.pokemon[i]+"! I choose you!");
		}
	}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation");
console.log(trainer['pokemon']);
trainer.talk(0);

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
    		if (this.health <= 0){
        console.log(this.name + ' fainted.');
        }
    }

}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
pikachu.faint();
mewtwo.tackle(geodude);
geodude.faint();
console.log(geodude);